# 使用データベースについて

## 概要
本研究ではSQL.jsという，SQLiteをJavaScriptに変換したnpmパッケージを使用する
SQL.jsは組み込みデータベースであり，使用PCで別途データベースのインストールや起動が必要ない
データの保存はインメモリもしくはファイル保存なのでデータの移植等が容易に可能である

## 使用方法
### インストール
```shell
$ npm install --save sql.js
```

### 空のデータベースの作成と基本的な使用方法
```javascript
// ライブラリの読み込み
const sql = require('sql.js')
// 新規データベースオブジェクトの作成
const db = new sql.Database()

// SQL文の実行
db.run('CREATE TABLE greet (id int, greet char);')
db.run("INSERT INTO greet VALUES (0, 'hello');")

// SELECTなどデータの返却があるSQL文の実行
let res = db.exec('SELECT * FROM greet')
```

### データベースファイルの読み込み
```javascript
const sql = require("sql.js")
const fs = require("fs")

let filebuffer = fs.readFileSync(データベースファイルへのパス)
let db = new sql.Database(filebuffer)
```

# 参考リンク
> * [SQL.js githubリポジトリ](https://github.com/kripken/sql.js/)
> * [SQL.js 公式DEMOページ](http://kripken.github.io/sql.js/GUI/)
> * [SQL.js 公式documentation](http://kripken.github.io/sql.js/documentation/#http://kripken.github.io/sql.js/documentation/class/Database.html)
