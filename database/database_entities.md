# データベースに必要なエンティティと型

## 入所者
|エンティティ名|型|Required|
|:-:|:-:|:-:|
|ID|uuid|True|
|氏名|String|True|
|性別|Enum|True|
|生年月日|Date|True|
|入所年月日|Date|True|
|居室|Number|True|
|入力時刻|Date|True|
|入力者|uuid|True|
|排便-大きさ|Enum|False|
|排便-形状|Enum|False|
|排便-色|Enum|False|
|投薬-種類|Enum|False|
|投薬-量|Number|False|
|血圧-上|Number|False|
|血圧-下|Number|False|
|食事-主食割合|Number|False|
|食事-副食割合|Number|False|
|水分-種類|Enum|False|
|水分-量|Number|False|
|体温|Number|False|
|排尿|Number|False|

## データ入力者
|エンティティ名|型|Required|
|:-:|:-:|:-:|
|ID|uuid|True|
|氏名|String|True|
|氏名|String|True|

## 居室
|エンティティ名|型|Required|
|:-:|:-:|:-:|
|部屋番号|Number|True|
|収容人数|Number|True|
