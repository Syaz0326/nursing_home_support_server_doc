# データベースのテーブル定義

## residents(入所者)
|エンティティ名|型|Required|その他|
|:-:|:-:|:-:|:-:|
|id|uuid|True|主キー|
|name|String|True|フルネーム|
|name_ruby|String|True|カタカナ フルネーム|
|sex|ENUM|True|1: male 2: female|
|birthday|Date|True||
|joined_at|Date|True||
|created_at|Date|True|入力時刻|
|update_at|Date|True|更新時刻|
|room_number|Number|True|居室テーブルへの外部キー|

## defecation(排便)
|エンティティ名|型|Required|その他|
|:-:|:-:|:-:|:-:|
|id|uuid|True|主キー|
|size|Enum|True|0: 微小 1: 少 2: 中 3:多|
|form|Enum|True|0: コロコロ・カチカチ 1: バナナ・半練り 2: 泥状・水状|
|color|Enum|True|0: 淡黄色 1: 黄褐色 2: 茶色 3: 褐色 4: 灰色 5: 黒色|
|created_at|Date|True|入力時刻|
|update_at|Date|True|更新時刻|
|resident_id|uuid|True|入所者への外部キー|
|staff_id|uuid|True|入力者テーブルへの外部キー|

## dosage(投薬)
|エンティティ名|型|required|その他|
|:-:|:-:|:-:|:-:|
|id|uuid|True|主キー|
|type|Enum|True|管理アプリケーションによって管理者が設定|
|dose|Number|True|投薬量|
|created_at|Date|True|入力時刻|
|update_at|Date|True|更新時刻|
|resident_id|uuid|True|入所者への外部キー|
|staff_id|uuid|True|入力者テーブルへの外部キー|

## blood_presure(血圧)
|エンティティ名|型|required|その他|
|:-:|:-:|:-:|:-:|
|id|uuid|True|主キー|
|top_number|number|True||
|bottom_number|number|True||
|created_at|Date|True|入力時刻|
|update_at|Date|True|更新時刻|
|resident_id|uuid|True|入所者への外部キー|
|staff_id|uuid|True|入力者テーブルへの外部キー|

## meal(食事)
|エンティティ名|型|required|その他|
|:-:|:-:|:-:|:-:|
|id|uuid|True|主キー|
|staple_food|Number|True|0~10|
|side_dish|Number|True|0~10|
|created_at|Date|True|入力時刻|
|update_at|Date|True|更新時刻|
|resident_id|uuid|True|入所者への外部キー|
|staff_id|uuid|True|入力者テーブルへの外部キー|

## hydrate(水分補給)
|エンティティ名|型|required|その他|
|:-:|:-:|:-:|:-:|
|id|uuid|True|主キー|
|type|Enum|True|管理者によって設定|
|amount|Number|True||
|created_at|Date|True|入力時刻|
|update_at|Date|True|更新時刻|
|resident_id|uuid|True|入所者への外部キー|
|staff_id|uuid|True|入力者テーブルへの外部キー|

## body_temperature(体温)
|エンティティ名|型|required|その他|
|:-:|:-:|:-:|:-:|
|id|uuid|True|主キー|
|temperature|Number|True||
|created_at|Date|True|入力時刻|
|update_at|Date|True|更新時刻|
|resident_id|uuid|True|入所者への外部キー|
|staff_id|uuid|True|入力者テーブルへの外部キー|

## urinate(排尿)
|エンティティ名|型|required|その他|
|:-:|:-:|:-:|:-:|
|id|uuid|True|主キー|
|volume|Number|True||
|created_at|Date|True|入力時刻|
|update_at|Date|True|更新時刻|
|resident_id|uuid|True|入所者への外部キー|
|staff_id|uuid|True|入力者テーブルへの外部キー|

## staffs(入力者)
|エンティティ名|型|required|その他|
|:-:|:-:|:-:|:-:|
|id|uuid|True|主キー|
|name|String|True|フルネーム|
|name_ruby|String|True|カタカナ フルネーム|
|permission|Enum|True|入力社(スタッフ)が持つ権限|
|created_at|Date|True|入力時刻|
|update_at|Date|True|更新時刻|

## rooms(居室)
|エンティティ名|型|required|その他|
|:-:|:-:|:-:|:-:|
|room_number|number|True|主キー 部屋番号|
|seating_capacity|number|True|収容人数|
|created_at|Date|True|入力時刻|
|update_at|Date|True|更新時刻|
