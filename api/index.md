# 本システムで提供するAPI一覧

## 概要
モバイルアプリケーション側とのやり取りを行うAPIの一覧

## 規則
* ケバブケースを用いる
* GETメソッドを用いる際は頭にgetをつける e.g. get-all-residents
* POSTメソッドを用いる際は頭にpostをつける e.g. post-blood-pressure
* PUTメソッドを用いる際は頭にputをつける e.g. put-blood-pressure
* GETメソッドを用いてモバイルアプリケーション側からデータを送信する場合はクエリリングを用いる e.g. get-resident-data?id=id
