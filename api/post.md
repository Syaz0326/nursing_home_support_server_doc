# Postメソッド一覧

* post-blood-pressure
BloodPressureのデータを追加
* post-body-temperature
BodyTemperatureのデータを追加
* post-meal
Mealのデータを追加
* post-hydrate
Hydrateのデータを追加
* post-defecation
Defecationのデータを追加
* post-dosage
Dosageのデータを追加
* post-urinate
Urinateのデータを追加
