# Getメソッド一覧

# クエリリングなし
* get-all-residents
入所者全員のid，名前，ふりがな，性別を取得
* get-all-rooms
施設にあるすべての部屋の部屋番号と収容可能人数を取得

# クエリリングあり
* get-resident-data?id=
クエリリングで入所者のidを渡すことでその入所者の今まで入力されたデータを取得
* get-blood-pressure?id=
クエリリングのidに対応する血圧データを1つ取得する
* get-body-temperature?id=
クエリリングのidに対応する体温データを1つ取得する
* get-meal?id=
クエリリングのidに対応する食事データを1つ取得する
* get-hydrate?id=
クエリリングのidに対応する水分データを1つ取得する
* get-defecation?id=
クエリリングのidに対応する排便データを1つ取得する
* get-dosage?id=
クエリリングのidに対応する投薬データを1つ取得する
* get-urinate?id=
クエリリングのidに対応する排尿データを1つ取得する
