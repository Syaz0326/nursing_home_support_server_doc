# 本アプリケーションの開発するための準備

## 必要なツール
* git
* node.js
* electron
etc...

## プロジェクトを動作可能にするまで
1. git(git bash)をインストール
  * [windows](https://qiita.com/toshi-click/items/dcf3dd48fdc74c91b409)
  * [gitの使い方](https://backlog.com/ja/git-tutorial/)
2. nodejs(v8.11.2)をインストール
  * [参考](https://qiita.com/taiponrock/items/9001ae194571feb63a5e)
3. gitlabアカウントを取得
4. [プロジェクトページ](https://gitlab.com/Syaz0326/nursing_home_support_server)にアクセスし，右上のForkをクリック
5. 自身のリポジトリ↑のリポジトリの複製が追加されるのでそのリポジトリページへアクセス
6. Forkボタンの隣にある青色の`clone`をクリック`Clone with HTTPS`の下にあるURLをコピー
7. git bash上でプロジェクトをダウンロードしたいディレクトリへ移動し以下を`を実行
```
git clone 'コピーしたURL'
```
8. ダウンロードしたディレクトリへ移動し以下を実行
```
$ npm install
```

